import service from '@/services/smart.service'

var data = service.get()

export const smart = {
  namespaced: true,
  state: {
    main: data,
  },
  mutations: {
    main: async (state, value) => {
      state.main = value;
    },
  },
  actions: {
    reset: async (context) => {
      context.commit('main', service.get());
    },
  },
}