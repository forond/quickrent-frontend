import service from '@/services/offers.service';

let data = null;

export const offers = {
  namespaced: true,
  state: {
    main: data,
    filters: data,
    smart: data,
  },
  mutations: {
    main: async (state, value) => {
      state.main = value
    },
    filters: async (state, value) => {
      state.filters = value
    },
    smart: async (state, value) => {
      state.smart = value
    }
  },
  actions: {
    main(context, props) {
      service.list(props.num, props.page, null).then(
        response => {
          context.commit('main', response);
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    filters(context, props) {
      service.list(props.num, props.page, props.filters).then(
        response => {
          context.commit('filters', response);
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
  }
}