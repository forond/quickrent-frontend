import service from '@/services/filters.service'

var data = service.get()

export const filters = {
  namespaced: true,
  state: {
    main: data
  },
  mutations: {
    main: async (state, value) => {
      state.main = value;
    },
  },
  actions: {
    reset: async (context) => {
      context.commit('main', service.get());
    },
    grouping: async (context, props) => {
      context.commit('main', service.grouping(props.type));
    },
  },
  getters: {
    quick: state => {
      let result = [];
      for (let i in state.main) result.push(state.main[i]);
      return result.filter(item => item.quick);
    },
    additional: state => {
      let result = [];
      for (let i in state.main) result.push(state.main[i]);
      return result.filter(item => !item.quick);
    },
    onlyValues: state => {
      let result = {};
      for (let i in state.main) result[i] = state.main[i].value;
      return result;
    }
  }
}