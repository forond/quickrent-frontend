import service from '@/services/pagination.service'

export const pagination = {
  namespaced: true,
  state: {
    main: 0,
    filters: 0,
    count: 0
  },
  mutations: {
    main: (state, data) => {
      state.main = Math.ceil(data / 12);
    },
    filters: (state, data) => {
      state.filters = Math.ceil(data / 12);
    },
    count: (state, data) => {
      state.count = data;
    }
  },
  actions: {
    count: (context) => {
      service.main().then(
        response => {
          context.commit('count', response)
          return response;
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    main: (context) => {
      service.main().then(
        response => {
          context.commit('main', response)
          return response;
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
    filters: (context, props) => {
      service.filters(props.filters).then(
        response => {
          context.commit('filters', response)
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    }
  }
}