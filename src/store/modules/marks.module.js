import service from '@/services/marks.service'

const value = null;

export const marks = {
  namespaced: true,
  state: {
    main: value
  },
  mutations: {
    main: (state, data) => {
      state.main = data
    }
  },
  actions: {
    main(context) {
      service.marks().then(
        response => {
          context.commit('main', response);
          return Promise.resolve(response);
        },
        error => {
          return Promise.reject(error);
        }
      );
    },
  }
}