import './registerServiceWorker';

import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'swiper/css/swiper.css';
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css';
import 'vue-slider-component/theme/default.css';
import '@/assets/styles/main.scss';


import { VLazyImagePlugin } from "v-lazy-image";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import vueScrollBehavior from 'vue-scroll-behavior'
import PerfectScrollbar from 'vue2-perfect-scrollbar';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import ToggleButton from 'vue-js-toggle-button';
import Vuelidate from 'vuelidate';

Vue.use(vueScrollBehavior, { router: router })
Vue.use(BootstrapVue, IconsPlugin);
Vue.use(VLazyImagePlugin);
Vue.use(VueAwesomeSwiper);
Vue.use(PerfectScrollbar);
Vue.use(ToggleButton);
Vue.use(Vuelidate);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
