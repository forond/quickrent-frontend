import Vue from 'vue'
import VueRouter from 'vue-router'

import axios from "axios";
import store from "@/store";

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    name: 'main-view',
    meta: { layout: 'MainLayout', auth: false },
    component: () => import('@/views/Main.vue')
  },
  {
    path: "/map",
    name: 'map-view',
    meta: { layout: 'MainLayout', auth: false },
    component: () => import('@/views/Map.vue')
  },
  {
    path: "/filters",
    name: 'filters-view',
    meta: { layout: 'MainLayout', auth: false },
    component: () => import('@/views/Filters.vue')
  },
  {
    path: "/settings",
    name: 'settings-view',
    meta: { layout: 'MainLayout', auth: true },
    component: () => import('@/views/Settings.vue')
  },
  {
    path: "/favorites",
    name: 'favorites-view',
    meta: { layout: 'MainLayout', auth: true },
    component: () => import('@/views/Favorites.vue')
  },
  {
    path: "/smart",
    name: 'smart-view',
    meta: { layout: 'MainLayout', auth: true },
    component: () => import('@/views/Smart.vue')
  },
  {
    path: "/offer/id:id",
    name: 'offer-view',
    meta: { layout: 'MainLayout', auth: false },
    component: () => import('@/views/Offer.vue')
  },
  {
    path: "/create",
    name: 'create-view',
    meta: { layout: 'MainLayout', auth: true },
    component: () => import('@/views/Create.vue')
  },
  {
    path: "/login",
    name: 'login-view',
    meta: { layout: 'EmptyLayout', auth: false },
    component: () => import('@/views/Login.vue')
  },
  {
    path: "/register",
    name: 'register-view',
    meta: { layout: 'EmptyLayout', auth: false },
    component: () => import('@/views/Register.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

router.beforeEach((to, from, next) => {
  const require = to.matched.some(record => record.meta.auth);
  const user = localStorage.getItem('user');

  if (require && !user) {
    next({ name: 'login-view' });
  } else {
    next();
  }
})

let refresh = false;

axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    const { config, response: { status, data } } = error;
    const request = config;

    if (status === 401 && data.message === "Invalid token") {
      if (!refresh) {
        refresh = true;
        console.log("Вызвана попытка восстановить токен.")
        store.dispatch("auth/refresh")
          .then(response => {
            if (response.data.accessToken && response.data.refreshToken) {
              localStorage.setItem("user", JSON.stringify(response.data));
            }
            console.log("Токен восстановлен.")
            return axios(request)
          })
          .catch(error => {
            console.log("Токен восстановлен.")
            localStorage.removeItem("user");
            store.dispatch("auth/logout");
            next({ name: "login-view" });
            return Promise.reject(error);
          })
        refresh = false;
      }
    }
    return Promise.reject(err);
  }
);

export default router
