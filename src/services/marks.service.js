import axios from "axios";

const api = "/api/all/offers/";

class MarksService {
  mark(id) {
    return axios
    .post(api + "detail/", {
      id: id
    })
    .then(response => {
      return response.data;
    });
  }

  marks() {
    return axios
    .post(api + "get-marks/")
    .then(response => {
      return response.data;
    });
  }
}

export default new MarksService();