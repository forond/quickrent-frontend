import axios from "axios";

const api = "/api/all/offers/";

class PaginationService {

  main() {
    return axios
      .post(api + "count/", {
        filters: null
      })
      .then(response => {
        return response.data;
      });
  }

  filters(filters) {
    return axios
      .post(api + "count/", {
        filters: filters
      })
      .then(response => {
        return response.data;
      });
  }
}

export default new PaginationService();
