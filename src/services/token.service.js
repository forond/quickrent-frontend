import axios from "axios";

class tokenService {
  static getToken() {
    let user = JSON.parse(localStorage.getItem('user'));
    if (user && user.accessToken) {
      return { Authorization: 'Bearer ' + user.accessToken };
    } else {
      return {};
    } 
  }
}

export default tokenService