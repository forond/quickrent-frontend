class SmartService {
  get() {
    var data = require('@/data/modules/smart');
    var result = JSON.parse(JSON.stringify(data));
    return result;
  }
}

export default new SmartService();